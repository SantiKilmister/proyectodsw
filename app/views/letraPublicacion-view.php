<?php 
require_once "partials/nav-partial.php";
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-4">

            <h1 class="my-4">
                <small>English</small>
            </h1>

            <?php require_once "partials/card-partialIngles.php"; ?>

                    

        </div>


        <div class="col-4">

            <h1 class="my-4">
                <small>Turkish</small>
            </h1>

            <?php require_once "partials/card-partialTurco.php"; ?>

                    

        </div>
        
    </div>


</div>









<?php

require_once "partials/footer-partial.php";
?>